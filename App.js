
import React, {useEffect} from 'react';
import firebase from '@react-native-firebase/app'
import AppNavigation from './src/navigation/navigation'



  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyCcTvE32ockhz-Wf2fj3iTOUWFOwhdeUok",
    authDomain: "sanbercode-fdf83.firebaseapp.com",
    databaseURL: "https://sanbercode-fdf83.firebaseio.com",
    projectId: "sanbercode-fdf83",
    storageBucket: "sanbercode-fdf83.appspot.com",
    messagingSenderId: "655826954055",
    appId: "1:655826954055:web:cb8e5b6b52eb5d37c1ce62",
    measurementId: "G-79Y0Q7P2R8"
  };
  // Initialize Firebase
  // firebase.initializeApp(firebaseConfig);
  // firebase.analytics();
  if(!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
  }
  

const App = () =>{
  return (
    <AppNavigation/>
    
  )
};


export default App;

