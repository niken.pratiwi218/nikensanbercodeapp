import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from '../screens/Splashscreen/Splashscreen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Intro from '../screens/Intro/Intro';
import Login from '../screens/Login/Login';
import Profile from '../screens/Profile'
import Register from '../screens/Register/Register';
import Home from '../screens/Home/HomePage';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Maps from '../screens/maps/maps'
import Details from '../screens/ReactNative/details';
import Chat from '../screens/Chat/chat';
import Ikon from 'react-native-vector-icons/Entypo';
import AsyncStorage from '@react-native-community/async-storage'
import { set } from 'react-native-reanimated';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const OnboardingNavigation = () => (
    <Stack.Navigator>
            <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="TabsScreen" component={TabsScreen} options={{ headerShown: false }} />
            <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
            <Stack.Screen name="React Native" component={Details} />
        </Stack.Navigator>
)

const HomeNavigation = () => (
    <Stack.Navigator>
            <Stack.Screen name="TabsScreen" component={TabsScreen} options={{ headerShown: false }} />
            <Stack.Screen name="React Native" component={Details} />
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        </Stack.Navigator>
)

const LoginNavigation = () => (
    <Stack.Navigator>
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="TabsScreen" component={TabsScreen} options={{ headerShown: false }} />
            <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
            <Stack.Screen name="React Native" component={Details} />
    </Stack.Navigator>
)   
const TabsScreen = () => {
    return(
    <Tabs.Navigator
        tabBarOptions={{
        activeTintColor: '#088dc4',
        showIcon: true
        }}>
        <Tabs.Screen name='Home' component={Home} 
            options={{
                tabBarIcon: () => (
                    <Icon name='home' size={25}/>
                )
            }} 
        />
         <Tabs.Screen name='Maps' component={Maps} 
            options={{
                tabBarIcon: () => (
                    <Icon name='google-maps'  size={25}/>
                )
            }} 
        />
        <Tabs.Screen name='Chat' component={Chat}
            options={{
                tabBarIcon: () => (
                    <Ikon name='chat' size={25} />
                )
            }}
        />
        <Tabs.Screen name='Profile' component={Profile}
            options={{
                tabBarIcon: ( ) => (
                    <Icon name='account-outline'  size={25}/>
            )
        }}
        />
    </Tabs.Navigator>
    )
}
function AppNavigation() {
    const [ isLoading, setIsLoading ] = React.useState(true)
    const [login, setLogin] =React.useState(false)
    const [onBoarding, setOnBoarding] = React.useState(false)

    const getstatus = async() => {
        const login = await AsyncStorage.getItem('login')
        const onBoarding = await AsyncStorage.getItem('Onboarding')
        setLogin(login)
        setOnBoarding(onBoarding)
        console.log(login)
        console.log(onBoarding)
    }
    React.useEffect(() => {
        getstatus()
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)
    }, [])

    if(isLoading) {
        return <SplashScreen />
    }

    if(login === 'true') {
        return (
            <NavigationContainer> 
                <HomeNavigation />
            </NavigationContainer>
        )
    } else if (onBoarding === 'true') {
        return (
            <NavigationContainer>
                <LoginNavigation />
            </NavigationContainer>
        )
    } 

    return (
        <NavigationContainer>
            <OnboardingNavigation />
        </NavigationContainer>
    )
    
}

export default AppNavigation;