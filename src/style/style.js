import React from 'react';
import { View, Image, StyleSheet } from "react-native";

const styles= StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  slide:{
    alignItems: 'center',
    marginVertical: 100
  },
  title:{
    fontSize: 25,
    color: '#191970',
    marginVertical: 50,
    fontWeight: 'bold'
  },
  text: {
    fontSize: 16,
    color: '#a4a4a6',
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 50
  },
  buttonCircle: {
    backgroundColor: '#191970',
    borderRadius: 50,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  content: {
    marginHorizontal: 20,
    height: '60%'
  },
  lineContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'space-between'
  },
  line: {
    borderWidth: 0.5,
    marginTop: 10,
    width: 165
  },
  btnFingerprint:{
    marginVertical: 20
  }
})

export default styles;