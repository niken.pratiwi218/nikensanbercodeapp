import React, { useState, createContext } from 'react'
import TodoList from '../screens//TodoList';


export const RootContext = createContext();

const Context = () => {
  
  const  [todo, setTodo] = useState ('')
  const [todoArray, setTodoArray] = useState([])

  handleChangeInput = (value) => {
    setTodo(value)
  }
  
  addTodo = () => {
    if (todo){
      var d = new Date();
      var newTodo = {
        'date': d.getFullYear() + 
        "/" + (d.getMonth() + 1) +
        "/" + d.getDate(),
        'todo': todo        };
      setTodoArray([...todoArray, newTodo ])
      setTodo('')
    }
  }

  deleteMethod = (key) => {
    todoArray.splice(key,1)
    setTodoArray([...todoArray])
  }

  return (
    <RootContext.Provider value={{
      todo,
      todoArray,
      handleChangeInput,
      addTodo,
      deleteMethod
    }}>
      <TodoList />
    </RootContext.Provider>
  )
}

export default Context;



