import React, {useState, useEffect} from 'react';
import {
    View,
    Text,
    Image,
    Button,
    TextInput,
    StatusBar,
    Alert,
} from 'react-native';
import styles from '../../style/style';
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import api from '../../api/index'
import auth from '@react-native-firebase/auth'
import {  statusCodes,  GoogleSignin, GoogleSigninButton } from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id'

const config = {
    title: 'Authentication Required',
    imageColor: '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'
}

function Login({ navigation }) {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('') 


    const saveToken = async(token)=>{
        try{
            await AsyncStorage.setItem('token', token)
        } catch(err) {
            console.log(err)
        }
    }

    useEffect(()=> {
        configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: '655826954055-c3d29np5ktu16tmk1nnm6jptg2skneoa.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle =  async () => {
        try{
            const { idToken } = await GoogleSignin.signIn()
            console.log('signInWithGoogle -> idToken', idToken)
            
            const credential = auth.GoogleAuthProvider.credential(idToken);

            auth().signInWithCredential(credential)
            await AsyncStorage.setItem('google', 'true')
            await AsyncStorage.setItem('login', 'true')

            navigation.reset({
                index: 0,
                routes: [{ name: 'TabsScreen'}],
            })

        } catch (error) {
            console.log('signInWithGoogle -> error', error)
        }
    }


    const onLoginPress = async() => {
        await AsyncStorage.setItem('login', 'true')
        navigation.reset({
            index: 0,
            routes: [{ name: 'TabsScreen'}],
        })
    }
    // const onLoginPress = () => {
    //     return auth().signInWithEmailAndPassword(email, password)
    //     .then((res) => {
    //         navigation.navigate('TabsScreen')
    //     })
    //     .catch((err) => {
    //         console.log('onLoginPress -> err', err)
    //     })
    // }


    // const onLoginPress = () => {
    //     let data = {
    //         email: email,
    //         password: password
    //     }
    //     console.log(data)
    //     Axios.post(`${api}/login`, data, {
    //         timeout: 20000
    //     })
    //     .then((res)=> {
    //         console.log(res)
    //         saveToken(res.data.token)
    //         navigation.navigate('TabsScreen')
    //     })
    //     .catch((err) => {
    //         console.log(err)
    //         console.log(Alert.alert('Login Failed', 'Your email or password is incorrect. Please try again.'))
    //     })        
    // }
    
    const signInWithFingerprint = () => {
        TouchID.authenticate('', config)
        .then(success => {
            AsyncStorage.setItem('login', 'true')
            navigation.reset({
                index: 0,
                routes: [{ name: 'TabsScreen'}],
            })
        })
        .catch(error => {
            console.log(error)
            alert('Authentication Failed')
        })
        
    }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <View style={styles.imageContainer}>
                <Image source={require('../../assets/images/logo.jpg')} style={styles.logo}/>
            </View>
            <View style={styles.content}>
                <View style={styles.formContainer}>
                    <Text>Username</Text>
                    <TextInput
                        value={email}
                        underlineColorAndroid='#c6c6c6'
                        placeholder='Username or Email'
                        onChangeText={(email) => setEmail(email)}
                    />
                    <Text>Password</Text>
                    <TextInput
                        secureTextEntry
                        value={password}
                        placeholder='Password'
                        underlineColorAndroid='#c6c6c6'
                        onChangeText={(password) => setPassword(password)}
                    />
                </View>
                <View style={styles.btnContainer}> 
                    <View>
                        <Button
                            color='#3EC6FF'
                            title='LOGIN'
                            onPress={() => onLoginPress()}
                        />
                    </View>
                    <View style={styles.lineContainer}>
                        <View style={styles.line}></View>
                        <Text style={{marginTop: 10}}>OR</Text>
                        <View style={styles.line}></View>
                    </View>
                    <View style={{ marginTop: 10}}>
                        <GoogleSigninButton
                            onPress={()=> signInWithGoogle()}
                            style={{ width: '100%', height: 40}}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Dark}
                        />
                    </View>
                    <View style={styles.btnFingerprint}>
                        <Button
                            color= '#191970'
                            title='SIGN IN WITH FINGERPRINT'
                            onPress={() => signInWithFingerprint()}
                            />
                    </View>
                </View>
                <Text style={{textAlign: 'center', marginTop: 30}} >Belum mempunyai akun ?
                        <Text style={{color: '#3EC6FF'}} onPress={()=> navigation.navigate('Register')}>  Buat Akun</Text>
                </Text>
                
            </View>
        </View>
    )
}

export default Login;