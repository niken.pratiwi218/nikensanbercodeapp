import React, {useContext} from 'react';
import { View, Text, StyleSheet,  FlatList,  TextInput, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';
import { RootContext } from '../context';

const  TodoList = () => {

    const state = useContext(RootContext)

    const notes = ( {item, index} ) => {
      return(
          <View key={index} style={styles.note}>
            <View styles={styles.noteLeftBox}>
              <Text style={styles.noteText}>{item.date}</Text>
              <Text style={styles.noteText}>{item.todo}</Text>
            </View>
            <View style={styles.noteRightBox}>
              <TouchableOpacity 
                onPress={()=>state.deleteMethod(index)} style={styles.noteDelete}>
                <Icon style = {styles.deleteIcon} name = 'trashcan' size= {25}/>
              </TouchableOpacity>
            </View>
          </View>
      )
    }

      return (
        <View style={styles.container}>
          <Text style={styles.textFormat}>
            Masukkan Todolist
          </Text>
          <View style={styles.subContainer}>
            <View style={styles.box}>
              <TextInput 
                style={styles.textInput}
                onChangeText={(value) => state.handleChangeInput(value)}
                value={state.todo}
                placeholder='Input here'
              />
            </View>

            <TouchableOpacity 
              style={styles.addButton}
              onPress={() => state.addTodo()}>
              <Text style={styles.addButtonText}>+</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={state.todoArray}
            renderItem={notes}
          />
        </View>
    )
};

const styles= StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  textFormat: {
    fontSize: 12,
    color:'black',
  },
  subContainer:{
    width: '100%',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10
  },
  box: {
    width: '85%',
    height: 50,
    borderColor: 'black',
    borderWidth: 1
  },
  textInput: {
    fontSize: 12
  },
  addButton: {
    width: 50,
    height: 50,
    backgroundColor: '#3EC6FF',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  addButtonText:{
    fontSize: 25
  },
  note: {
    position: 'relative',
    padding: 15,
    borderWidth: 5,
    borderColor: '#b5b2b0',
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 5
  },
  noteDelete: {
    
  },
})

export default TodoList;