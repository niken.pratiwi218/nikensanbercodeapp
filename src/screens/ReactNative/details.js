import React, {useState} from "react";
import { View, Text, StyleSheet, Button, processColor } from "react-native";
import { BarChart } from 'react-native-charts-wrapper'
import { blue, darkblue } from '../../style/colors'

const Details = ({navigation}) => {
 
  const data1 = [
    {y:[100, 40], marker: ["React Native Dasar", "React Native Lanjutan"]},
    {y:[80, 60], marker: ["React Native Dasar", "React Native Lanjutan"]},
    {y:[40, 90], marker: ["React Native Dasar", "React Native Lanjutan"]},
    {y:[78, 45], marker: ["React Native Dasar", "React Native Lanjutan"]},
    {y:[67, 87], marker: ["React Native Dasar", "React Native Lanjutan"]},
    {y:[98, 32], marker: ["React Native Dasar", "React Native Lanjutan"]},
    {y:[150, 90], marker: ["React Native Dasar", "React Native Lanjutan"]},
  ]
  
  const newDataFormat = []
  data1.map((item,index) => newDataFormat.push({y: item.y, marker:[`${item.marker[0]} \n ${item.y[0]}`,`${item.marker[1]} \n ${item.y[1]}`]}))

  console.log(newDataFormat)
  

const [legend, setLegend] = useState({
  enabled: true,
  textSize: 14,
  form: 'SQUARE',
  formSize: 14,
  xEntrySpace: 10,
  yEntrySpace: 5,
  formToTextSpace: 5,
  wordWrapEnabled: true,
  maxSizePercent: 0.5
})

const [chart, setChart] = useState({
  data: {
      dataSets: [{  
          values: newDataFormat,
          label: '',
          config: {
              colors: [processColor(blue), processColor(darkblue)],
              stackLabels: ['React Native Dasar', 'React Native Lanjutan'],
              drawFilled: false,
              drawValues: false,
          }
      }]
  }
})

console.log(chart)

const [xAxis, setXAxis] = useState({
  valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Agu'],
  position: 'BOTTOM',
  drawAxisLine: true,
  drawGridLines: false,
  axisMinimum: -0.5,
  granularityEnabled: true,
  granularity: 1,
  axisMaximum: new Date().getMonth(),
  spaceBetweenLabels: 0,
  labelRotationAngle: -45.0,
  limitLines: [{ limit: 115, lineColor: processColor('red'), lineWidth: 1 }]
})
const [yAxis, setYAxis] = useState({
  left: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      drawGridLines: false
  },
  right: {
      axisMinimum: 0,
      labelCountForce: true,
      granularity: 5,
      granularityEnabled: true,
      enabled: false
  }
})
  return(
    <View style={styles.container}>
      <BarChart 
        style={{flex: 1}}
        data={chart.data}
        yAxis={yAxis}
        xAxis={xAxis}
        pinchZoom={false}
        doubleTapToZoomEnabled={false}
        chartDescription={{ text: '' }}
        legend={legend}
        marker={{
          enabled: true,
          color: 'grey',
          textColor: '#ffffff',
          textSize: 14
        }}
      />
    </View>
  )
}


const styles=StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff'
  }
})
export default Details;