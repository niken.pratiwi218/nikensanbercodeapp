import React, {useEffect,useState} from 'react';
import { View, Text, StyleSheet, Image, Button } from 'react-native';
import Axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import api from '../api'
import { statusCodes, GoogleSignin } from '@react-native-community/google-signin'


function Profile({navigation}) {
    const [userInfo, setUserInfo] = useState(null)

    useEffect(() => {
      async function getToken() {
        try{
          const token =await AsyncStorage.getItem('token')
          const login = await AsyncStorage.getItem('login')
          const google = await AsyncStorage.getItem('google')
        } catch (err){
          console.log(err)
        }
      }
      getToken()
      getCurrentUser()
    }, [userInfo])
    

    const getCurrentUser = async() => {
      const userInfo = await GoogleSignin.signInSilently()
      setUserInfo(userInfo)
    }

    const getVenue =(token) =>{
      Axios.get(`${api}/venues`, {
        timeout: 20000,
        headers: {
          'Authorization' : 'Bearer' + token
        }
      })
      .then((res) => {
        console.log('Profile -> res', res)
      })
      .catch((err)=> {
        console.log('Profile -> err', err)
      }) 
    }

    const onLogoutPress = async () =>{
      const google = await AsyncStorage.getItem('google')

      console.log(google)
      
      try{
        if (google === 'true'){
          await GoogleSignin.revokeAccess()
          await GoogleSignin.signOut()
          await AsyncStorage.removeItem('login')
          await AsyncStorage.removeItem('google')
          navigation.navigate ('Login')
        } else{
          await AsyncStorage.removeItem('login')
          await AsyncStorage.removeItem('token')
          navigation.navigate ('Login')
        }
      }catch(err){
        console.log(err)
      }
    }
    
   const blankImage = `https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png`

    return (
        <View style={styles.container}>
          <View style={styles.containerPhoto} >
            <Image source={{uri:`${userInfo ? userInfo && userInfo.user && userInfo.user.photo : blankImage}`}} style={{width: 100, height: 100, borderRadius: 100}} />
            <Text style={{fontSize: 16, color: 'white', marginTop: 15, fontWeight: '700' }}>{userInfo && userInfo.user && userInfo.user.name}</Text>
          </View>
          
        <View style={styles.contentContainer}>
        <View style={styles.containerData}>
            <View style={styles.subContainerDataInput}>
              <Text style={styles.textProfile}>Tanggal Lahir</Text>
              <Text style={styles.textProfile}>Jenis Kelamin</Text>
              <Text style={styles.textProfile}>Hobi</Text>
              <Text style={styles.textProfile}>No. Telp</Text>
              <Text style={styles.textProfile}>Email</Text>
            </View>
            
            <View style={styles.subContainerDataProfile}>
              <Text style={styles.textProfile}>21 Agustus 1997</Text>
              <Text style={styles.textProfile}>Perempuan</Text>
              <Text style={styles.textProfile}>Ngoding</Text>
              <Text style={styles.textProfile}>081380309997</Text>
              <Text style={styles.textProfile}>{userInfo && userInfo.user && userInfo.user.email}</Text>
            </View>
          </View>

          <View style={styles.btnContainer}> 
            <Button
               color='#3EC6FF'
               title='LOGOUT'
               onPress={() => onLogoutPress()}
            />
          </View>
        </View>
        </View>
    )
};

const styles= StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  containerPhoto: {
    width: '100%',
    height: 250,
    backgroundColor: '#3EC6FF',
    justifyContent: 'center',
    alignItems: 'center'
  },
  contentContainer:{
    width: '90%',
    backgroundColor: 'white',
    marginTop: -30,
    borderRadius: 10,
    paddingHorizontal: 15,
    paddingTop: 10,
    paddingBottom: 20,
    alignSelf: 'center',
    elevation: 5 
  },
  containerData: {
    flexDirection: 'row',
    justifyContent: 'space-between',
     
  },
  textProfile: {
    marginVertical: 10,
    fontSize: 14
  },
  subContainerDataProfile: {
    alignItems: 'flex-end'
  }
})

export default Profile;