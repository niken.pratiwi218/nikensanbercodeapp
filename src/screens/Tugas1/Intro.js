import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class Intro extends React.Component {
  render(){
    return (
      <View style={styles.container}>
        <Text style= {styles.IntroTittle}>
        Hallo Kelas React Native Lanjutan Sanbercode!
        </Text>
      </View>
    )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  IntroTittle: {
    fontSize: 14,
    color: 'black'
  }
})