import React, {useEffect,useState} from 'react';
import { View, Text, StyleSheet, Image, Button, TextInput, Modal, TouchableOpacity } from 'react-native';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import storage from '@react-native-firebase/storage';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';

function Register({navigation}) {
    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)

    const toggleCamera = () => {
      setType(type === 'back' ? 'front' : 'back')
    }
   
    const takePicture = async () => {
      const options = { quality: 0.5, base64: true}
      if(camera) {
        const data = await camera.takePictureAsync(options)
        setPhoto(data)
        setIsVisible(false)
      }
    }

    const uploadImage = (uri) => {
      const sessionId = new Date().getTime()
      return storage()
      .ref(`images/${sessionId}`)
      .putFile(uri)
      .then((response) => {
        alert('Upload Succes')
      })
      .catch((error) => {
        alert(error)
      })
    }
    const renderCamera = ()=> {
    return ( 
      <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
        <View style={{flex: 1}}>
          <RNCamera
            style={{ flex: 1}}
            ref={ref => {
              camera = ref;
            }}
            type={type}
          >
            <View style={styles.btnFlipContainer}>
              <TouchableOpacity style={styles.btnFlip} onPress={()  => toggleCamera()}>
                <MaterialCommunity name='rotate-3d-variant' size={20}/>
              </TouchableOpacity>
            </View>
            <View style={styles.round}/>
            <View style={styles.rectangle} />
            <View style={styles.btnTakeContainer}>
              <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                <Icon name='camera' size={40}/>
              </TouchableOpacity>
            </View>
          </RNCamera>
        </View>
      </Modal>
    )
    }

    return (
      <View style={styles.container}>
         {renderCamera()}
          <View style={styles.containerPhoto} >
            <Image source={photo === null ? require('../../assets/images/blank-photo-profile.png') : {uri: photo.uri}} style={{width: 100, height: 100, borderRadius: 100}} />
            <TouchableOpacity style={styles.changePicture} onPress={()=> setIsVisible(true)}>
              <Text style={{fontSize: 16, color: 'white', marginTop: 20, fontWeight: '700' }}>Change Picture</Text>
            </TouchableOpacity>
          </View>
          
        <View style={styles.contentContainer}>
              <Text style={{fontWeight: 'bold'}}>Nama</Text>
              <TextInput
                underlineColorAndroid='#c6c6c6'
                placeholder='Name'
              />
              <Text style={{fontWeight: 'bold'}}>Email</Text>
              <TextInput
                underlineColorAndroid='#c6c6c6'
                placeholder='Email'
              />
              <Text style={{fontWeight: 'bold'}}>Password</Text>
              <TextInput
                placeholder='Password'
                underlineColorAndroid='#c6c6c6'
              />
          <View style={styles.btnContainer}> 
            <Button
               color='#3EC6FF'
               title='REGISTER'
               onPress={() => uploadImage(photo.uri)}
            />
          </View>
        </View>
        <Text style={{textAlign: 'center', marginTop: 250}} >Sudah memiliki akun ?
            <Text style={{color: '#3EC6FF'}} onPress={()=> navigation.navigate('Login')}>  Log in</Text>
        </Text>
      </View>
    )
};

const styles= StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  containerPhoto: {
    width: '100%',
    height: 250,
    backgroundColor: '#3EC6FF',
    justifyContent: 'center',
    alignItems: 'center'
  },
  contentContainer:{
    width: '90%',
    backgroundColor: 'white',
    marginTop: -30,
    borderRadius: 10,
    padding: 20,
    alignSelf: 'center',
    elevation: 5 
  },
  btnFlipContainer: {
    backgroundColor: 'white',
    width: 50,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent:'center',
    margin: 20
  },
  round: {
    height: 330,
    width: 230,
    borderRadius: 150,
    borderColor: 'white',
    borderWidth: 1,
    marginBottom: 60,
    alignSelf: 'center'
  },
  rectangle: {
    height: 150,
    width: 230,
    borderColor: 'white',
    borderWidth: 1,
    alignSelf: 'center',
    marginBottom: 60
  },
  btnTakeContainer: {
    backgroundColor: 'white',
    width: 80,
    height: 80,
    borderRadius: 80,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  }
})

export default Register;