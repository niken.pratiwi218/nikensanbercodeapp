import React, {useState} from 'react';
import { View, Text, StyleSheet, ScrollView, TextInput, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Octicons';

const  TodoList = () => {
    const [todoArray, setTodoArray] = useState([])
    const  [todo, setTodo] = useState ('')

    const notes = (date, todo, deleteMethod, keyval) => {
      return(
          <View key={keyval} style={styles.note}>
            <View styles={styles.noteLeftBox}>
              <Text style={styles.noteText}>{date}</Text>
              <Text style={styles.noteText}>{todo}</Text>
            </View>
            <View style={styles.noteRightBox}>
              <TouchableOpacity onPress={()=>deleteMethod(keyval)} style={styles.noteDelete}>
                <Icon style = {styles.deleteIcon} name = 'trashcan' size= {25}/>
              </TouchableOpacity>
            </View>
          </View>
      )
    }

    const addTodo = () => {
      if (todo){
        var d = new Date();
        var newTodo = {
          'date': d.getFullYear() + 
          "/" + (d.getMonth() + 1) +
          "/" + d.getDate(),
          'todo': todo        };
        setTodoArray([...todoArray, newTodo])
        setTodo({ todo: '' })
        console.log(todoArray)
      }
    }

    const deleteMethod = (key) => {
      todoArray.splice(key,1)
      setTodoArray([...todoArray])
    }

      return (
        <View style={styles.container}>
          <Text style={styles.textFormat}>
            Masukkan Todolist
          </Text>

          <View style={styles.subContainer}>
            <View style={styles.box}>
              <TextInput 
                style={styles.textInput}
                onChangeText={(todo) => setTodo(todo)}
                value={todo}
                placeholder='Input here'
              />
            </View>

            <TouchableOpacity 
              onPress={addTodo}
              style={styles.addButton}>
              <Text style={styles.addButtonText}>+</Text>
            </TouchableOpacity>
          </View>
          <ScrollView>
            {todoArray.map((val, key)=> {
              return notes(val.date, val.todo, deleteMethod, key)
            })}
          </ScrollView>
          


        </View>
    )
};

const styles= StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  textFormat: {
    fontSize: 12,
    color:'black',
  },
  subContainer:{
    width: '100%',
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 10
  },
  box: {
    width: '85%',
    height: 50,
    borderColor: 'black',
    borderWidth: 1
  },
  textInput: {
    fontSize: 12
  },
  addButton: {
    width: 50,
    height: 50,
    backgroundColor: '#3EC6FF',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  addButtonText:{
    fontSize: 25
  },
  note: {
    position: 'relative',
    padding: 15,
    borderWidth: 5,
    borderColor: '#b5b2b0',
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 5
  },
  noteDelete: {
    
  },
})

export default TodoList;