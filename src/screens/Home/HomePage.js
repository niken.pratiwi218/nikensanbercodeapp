import React from 'react';
import { View, Text, ScrollView, StyleSheet, Image, TouchableOpacity} from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon from 'react-native-vector-icons/Ionicons';
import styles from './style'



function Home ({navigation}) {
  return (
  <ScrollView>
    <View style={styles.container}>
      <View style={styles.containerKelas}>
        <Text style={styles.titleContainer}>Kelas</Text>
        <View style={styles.containerKelasIcon}>
          <TouchableOpacity style={styles.boxKelas} onPress={() => navigation.navigate('React Native') } >
          <Icon name='logo-react'size={50} style={styles.icon}/>
          <Text style={styles.titleIcon}>React Native</Text>
          </TouchableOpacity>
          <View style={styles.boxKelas}>
            <Icon name='logo-python'size={50} style={styles.icon}/>
            <Text style={styles.titleIcon}>Data Science</Text>
          </View>
          <View style={styles.boxKelas}>
            <Icon name='logo-react'size={50} style={styles.icon}/>
            <Text style={styles.titleIcon}>React JS</Text>
          </View>
          <View style={styles.boxKelas}>
            <Icon name='logo-laravel'size={50} style={styles.icon}/>
            <Text style={styles.titleIcon}>Laravel</Text>
          </View>
        </View>
      </View>
      <View style={styles.containerKelas}>
        <Text style={styles.titleContainer}>Kelas</Text> 
        <View style={styles.containerKelasIcon}>
          <View style={styles.boxKelas}>
            <Icon name='logo-wordpress'size={50} style={styles.icon}/>
            <Text style={styles.titleIcon}>Wordpress</Text>
          </View>
          <View style={styles.boxKelas}>
            <Image source={require('../../assets/icons/website-design.png')} style={styles.iconImage}/>
            <Text style={styles.titleIcon}>Design Grafis</Text>
          </View>
          <View style={styles.boxKelas}>
            <MaterialCommunityIcons name='server'size={55} style={styles.icon}/>
            <Text style={styles.titleIcon}>Web Server</Text>
          </View>
          <View style={styles.boxKelas}>
            <Image source={require('../../assets/icons/ux.png')} style={styles.iconImage}/>
            <Text style={styles.titleIcon}>UI/UX Design</Text>
          </View>
        </View>
      </View>
      <View style={styles.containerSummary}>
        <Text style={styles.titleContainer}>Summary</Text> 
          <View style={styles.summaryKelas}>
            <Text style={styles.titleSummaryKelas}>React Native</Text>
            <View style={styles.containerSummaryDetail}>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Today</Text>
              <Text style={styles.detail}>20 orang</Text>
              </View>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Total</Text>
              <Text style={styles.detail}>100 orang</Text>
              </View>
            </View>
          </View>
          <View style={styles.summaryKelas}>
            <Text style={styles.titleSummaryKelas}>Data Science</Text>
            <View style={styles.containerSummaryDetail}>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Today</Text>
              <Text style={styles.detail}>30 orang</Text>
              </View>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Total</Text>
              <Text style={styles.detail}>100 orang</Text>
              </View>
            </View>
          </View>
          <View style={styles.summaryKelas}>
            <Text style={styles.titleSummaryKelas}>ReactJS</Text>
            <View style={styles.containerSummaryDetail}>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Today</Text>
              <Text style={styles.detail}>66 orang</Text>
              </View>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Total</Text>
              <Text style={styles.detail}>100 orang</Text>
              </View>
            </View>
          </View>
          <View style={styles.summaryKelas}>
            <Text style={styles.titleSummaryKelas}>Laravel</Text>
            <View style={styles.containerSummaryDetail}>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Today</Text>
              <Text style={styles.detail}>60 orang</Text>
              </View>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Total</Text>
              <Text style={styles.detail}>100 orang</Text>
              </View>
            </View>
          </View>
          <View style={styles.summaryKelas}>
            <Text style={styles.titleSummaryKelas}>Wordpress</Text>
            <View style={styles.containerSummaryDetail}>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Today</Text>
              <Text style={styles.detail}>45 orang</Text>
              </View>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Total</Text>
              <Text style={styles.detail}>100 orang</Text>
              </View>
            </View>
          </View>
          <View style={styles.summaryKelas}>
            <Text style={styles.titleSummaryKelas}>Design Grafis</Text>
            <View style={styles.containerSummaryDetail}>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Today</Text>
              <Text style={styles.detail}>30 orang</Text>
              </View>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Total</Text>
              <Text style={styles.detail}>100 orang</Text>
              </View>
            </View>
          </View>
          <View style={styles.summaryKelas}>
            <Text style={styles.titleSummaryKelas}>Web Server</Text>
            <View style={styles.containerSummaryDetail}>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Today</Text>
              <Text style={styles.detail}>50 orang</Text>
              </View>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Total</Text>
              <Text style={styles.detail}>100 orang</Text>
              </View>
            </View>
          </View>
          <View style={styles.summaryKelas}>
            <Text style={styles.titleSummaryKelas}>UI/UX Design</Text>
            <View style={styles.containerSummaryDetail}>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Today</Text>
              <Text style={styles.detail}>66 orang</Text>
              </View>
              <View style={styles.boxDetail}>
              <Text style={styles.detail}>Total</Text>
              <Text style={styles.detail}>100 orang</Text>
              </View>
            </View>
          </View>
      </View>
    </View>
  </ScrollView>
  )
}

export default Home;