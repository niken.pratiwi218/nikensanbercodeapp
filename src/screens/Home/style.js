import React from 'react';
import {StyleSheet} from 'react-native'



const styles=StyleSheet.create({
  container:{
    flex: 1,
    padding: 10
  },
  containerKelas:{
    height: 120,
    width: '100%',
    backgroundColor: '#088dc4',
    borderRadius: 10, 
    marginBottom: 10,
    justifyContent: 'flex-end'
  },
  containerSummary: {
    width: '100%',
    backgroundColor: '#088dc4',
    borderRadius: 10,
  },
  titleContainer: {
    color: '#ffffff',
    paddingVertical: 3,
    paddingHorizontal: 10,
    alignSelf: 'flex-start'
  },
  containerKelasIcon: {
    width: '100%',
    height: '80%',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor:'#3EC6FF',
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  containerSummaryIcon: {
    width: '100%',
    height: '80%',
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor:'#3EC6FF',
  },
  boxKelas: {
    height: '100%' ,
    width: 90,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon:{
    color: '#ffffff'
  },
  titleIcon: {
    color: '#ffffff',
  },
  iconImage: {
    width: 55,
    height: 55
  },
  summaryKelas: {
    width: '100%',
    height: 80,
    backgroundColor: '#3EC6FF',
    justifyContent: 'flex-end',
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10
  },
  titleSummaryKelas: {
    fontWeight: 'bold',
    paddingLeft: 10,
    paddingBottom: 5,
    fontSize: 12,
    color: '#ffffff'
  },
  containerSummaryDetail:{
    height: '70%',
    width: '100%',
    backgroundColor: '#088dc4',
    paddingHorizontal: 40,
    paddingVertical: 2,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10
  },
  detail: {
    fontSize: 12,
    color: '#ffffff',
  },
  boxDetail:{
    flexDirection:'row',
    justifyContent: 'space-between',
    marginTop: 5 
  }
})

export default styles;